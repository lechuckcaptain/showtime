package be.shouldit.showtime.ui.viewholder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import be.shouldit.showtime.R;
import be.shouldit.showtime.data.model.Employee;
import be.shouldit.showtime.ui.adapters.EmployeeAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Marco on 06/03/16.
 */
public class EmployeeViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.name)
    TextView employeeTextView;

    @BindView(R.id.selected)
    CheckBox selectionCheckBox;

    private final Context _context;
    private final EmployeeAdapter _employeeAdapter;
    private Employee _employee;
    private Boolean _isChecked;

    public EmployeeViewHolder(EmployeeAdapter adapter, View view, Context context) {

        super(view);

        _context = context;
        _employeeAdapter = adapter;
        ButterKnife.bind(this, view);
    }

    public void setEmployee(Employee employee, Boolean isChecked) {

        _employee = employee;
        _isChecked = isChecked;

        setName(_employee);
        setChecked(_isChecked);
    }

    public void setName(Employee employee) {
        this.employeeTextView.setText(employee.toString());
    }

    public void setChecked(Boolean isChecked) {

        this.selectionCheckBox.setChecked(isChecked);
    }

    @OnClick({R.id.cardview_layout, R.id.selected})
    public void selectedEmployee() {

        _employeeAdapter.onEmployeeSelected(_employee);
    }
}
