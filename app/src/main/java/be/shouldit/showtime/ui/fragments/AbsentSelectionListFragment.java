package be.shouldit.showtime.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import be.shouldit.showtime.R;
import be.shouldit.showtime.data.model.Presentation;
import be.shouldit.showtime.ui.OnEmployeeSelected;
import be.shouldit.showtime.ui.PresentationUpdate;
import be.shouldit.showtime.ui.adapters.EmployeeAdapter;
import be.shouldit.showtime.utils.UIUtils;
import butterknife.BindView;

public class AbsentSelectionListFragment extends BaseFragment {

    private static final String PRESENTATION = "PRESENTATION";
    private Presentation _presentation;
    private PresentationUpdate _pu;

    @BindView(R.id.footer)
    View footer;

    @BindView(R.id.list)
    RecyclerView _recyclerView;

    @BindView(R.id.no_data_text)
    TextView _noDataText;

    @BindView(R.id.fragment_container)
    RelativeLayout _fragmentContainer;

    private EmployeeAdapter mAdapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public AbsentSelectionListFragment() {

    }

    public static AbsentSelectionListFragment newInstance() {

        AbsentSelectionListFragment fragment = new AbsentSelectionListFragment();
        return fragment;
    }

    @Override
    protected void startProgress() {

        super.startProgress();

        UIUtils.setVisible(_recyclerView, false);
        UIUtils.setVisible(_noDataText, false);
    }

    @Override
    public void endProgress() {

        super.endProgress();

        boolean dataAvailable = mAdapter.getItemCount() > 0;

        UIUtils.setVisible(_recyclerView, dataAvailable);
        UIUtils.setVisible(_noDataText, !dataAvailable);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        _pu = ((PresentationUpdate) getActivity());
        _presentation = _pu.get_selected();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_checkable_employee_list, container, false);
        butterknifeBind(this, view);
        setupUI();
        return view;
    }

    private void setupUI() {

        startProgress();

        footer.setVisibility(View.VISIBLE);

        Context context = _recyclerView.getContext();
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        mAdapter = new EmployeeAdapter((OnEmployeeSelected) getActivity());
        _recyclerView.setAdapter(mAdapter);
        _recyclerView.setHasFixedSize(true);
        _recyclerView.setItemAnimator(new DefaultItemAnimator());
        _recyclerView.setLayoutManager(layoutManager);

        mAdapter.addAll(_presentation.get_available());
        mAdapter.setExcluded(_presentation.get_excluded());

        endProgress();
    }

}

