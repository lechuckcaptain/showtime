package be.shouldit.showtime.ui.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.StringRes;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import be.shouldit.showtime.R;
import be.shouldit.showtime.ui.fragments.BaseFragment;
import butterknife.BindView;
import timber.log.Timber;

public abstract class BaseActivity extends AppCompatActivity implements BaseFragment.OnFragmentInteractionListener {

    static final
    @LayoutRes
    int BASE_LAYOUT_ACTIVITY = R.layout.base_activity;
    static final
    @LayoutRes
    int DRAWER_LAYOUT_ACTIVITY = R.layout.activity_main;
    @BindView(R.id.content_coordinator_layout)
    protected CoordinatorLayout coordinatorLayout;
    @BindView(R.id.toolbar)
    protected Toolbar toolbar;

    protected void setupActionBar(@StringRes int title) {

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(false);
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setDisplayUseLogoEnabled(false);
            actionBar.setTitle(getString(title));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {

            Intent upIntent = NavUtils.getParentActivityIntent(this);
            if (upIntent != null)
                NavUtils.navigateUpTo(this, upIntent);
            else {
                Timber.e("Cannot find parentActivity for this: '%s'", this.getLocalClassName());
                NavUtils.navigateUpTo(this, new Intent(this, ListPresentationActivity.class));
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
