package be.shouldit.showtime.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import be.shouldit.showtime.R;
import be.shouldit.showtime.data.model.Presentation;
import be.shouldit.showtime.ui.PresentationUpdate;
import butterknife.BindView;
import butterknife.OnClick;

public class PresentationDetailsFragment extends BaseFragment implements CalendarDatePickerDialogFragment.OnDateSetListener, RadialTimePickerDialogFragment.OnTimeSetListener {

    private static final String PRESENTATION = "PRESENTATION";
    private static final String FRAG_TAG_DATE_PICKER = "DATE";
    private static final String FRAG_TAG_TIME_PICKER = "TIME";

    private Presentation _presentation;
    private PresentationUpdate _pu;

    @BindView(R.id.p_e_date)
    TextView dateTextView;

    @BindView(R.id.p_e_time)
    TextView timeTextView;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public PresentationDetailsFragment() {

    }

    public static PresentationDetailsFragment newInstance() {

        PresentationDetailsFragment fragment = new PresentationDetailsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        _pu = ((PresentationUpdate) getActivity());
        _presentation = _pu.get_selected();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_presentation_details, container, false);
        butterknifeBind(this, view);
        setupUI();
        return view;
    }

    private void setupUI() {

        startProgress();

        updateDateTime();

        endProgress();
    }

    private void updateDateTime() {

        dateTextView.setText(DateFormat.getDateInstance().format(_presentation.get_date()));
        timeTextView.setText(new SimpleDateFormat("h:mm a", Locale.getDefault()).format(_presentation.get_date()));

        _pu.onPresentationUpdated(_presentation);
        _presentation = _pu.get_selected();
    }

    @OnClick(R.id.p_e_date)
    public void setDate()
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(_presentation.get_date());

        CalendarDatePickerDialogFragment cdp = new CalendarDatePickerDialogFragment()
                .setOnDateSetListener(this)
                .setFirstDayOfWeek(Calendar.MONDAY)
                .setPreselectedDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        cdp.show(getFragmentManager(), FRAG_TAG_DATE_PICKER);
    }

    @OnClick(R.id.p_e_time)
    public void setTime()
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(_presentation.get_date());

        RadialTimePickerDialogFragment rtpd = new RadialTimePickerDialogFragment()
                .setOnTimeSetListener(this)
                .setForced12hFormat()
                .setStartTime(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE));
        rtpd.show(getFragmentManager(), FRAG_TAG_TIME_PICKER);
    }

    @Override
    public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(_presentation.get_date());

        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, monthOfYear);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        _presentation.set_date(calendar.getTime());
        updateDateTime();
    }

    @Override
    public void onTimeSet(RadialTimePickerDialogFragment dialog, int hourOfDay, int minute) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(_presentation.get_date());

        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);

        _presentation.set_date(calendar.getTime());
        updateDateTime();
    }
}

