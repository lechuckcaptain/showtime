package be.shouldit.showtime.ui.viewholder;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import be.shouldit.showtime.R;
import be.shouldit.showtime.data.model.Employee;
import be.shouldit.showtime.data.model.Presentation;
import be.shouldit.showtime.ui.activities.EditPresentationActivity;
import be.shouldit.showtime.ui.adapters.PresentationAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Marco on 06/03/16.
 */
public class PresentationViewHolder extends RecyclerView.ViewHolder {


    @BindView(R.id.p_li_presenter)
    TextView presenterTextView;

    @BindView(R.id.p_li_date)
    TextView dateTextView;

    @BindView(R.id.p_li_participants)
    TextView excludedTextView;

    @BindView(R.id.p_li_participants_count)
    TextView excludedCountTextView;

    private Presentation _presentation;
    private final Context _context;
    private final PresentationAdapter _presentationAdapter;

    public PresentationViewHolder(PresentationAdapter presentationAdapter, View view, Context context) {

        super(view);

        _context = context;
        _presentationAdapter = presentationAdapter;
        ButterKnife.bind(this, view);
    }

    @Override
    public String toString() {

        return super.toString() + " '" + presenterTextView.getText() + "'";
    }

    public void setPresentation(Presentation presentation) {

        _presentation = presentation;

        setPresenter(presentation.get_presenter());
        setExcluded(presentation.get_excluded());
        setDate(presentation.get_date());
    }

    private void setDate(Date date) {

        DateFormat df = DateFormat.getDateTimeInstance();
        this.dateTextView.setText(df.format(date));
    }

    public void setExcluded(Map<Employee, Date> excluded) {

        int excludedCount = 0;
        List<String> ll = new ArrayList<>();
        for(Map.Entry<Employee,Date> p : excluded.entrySet()) {
            if (p.getValue() != null) {
                ll.add(String.format("%s excluded on %s",
                        p.getKey().toString(),
                        DateFormat.getDateInstance().format(p.getValue())));
                excludedCount++;
            }
        }
        String text = TextUtils.join("\n", ll);

        this.excludedTextView.setText(text);
        this.excludedCountTextView.setText(String.format(_context.getString(R.string.participants), excludedCount));
    }

    public void setPresenter(Employee presenter) {
        this.presenterTextView.setText(presenter.get_firstName() + "" + presenter.get_lastName());
    }

    @OnClick(R.id.p_li_cardview_layout)
    public void selectedPresentation() {

        Intent i = new Intent(_context, EditPresentationActivity.class);
        Bundle b = new Bundle();
        b.putSerializable(EditPresentationActivity.SELECTED_PRESENTATION, _presentation);
        _context.startActivity(i,b);
    }
}
