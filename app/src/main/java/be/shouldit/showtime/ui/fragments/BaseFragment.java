package be.shouldit.showtime.ui.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ProgressBar;

import be.shouldit.showtime.R;
import be.shouldit.showtime.utils.UIUtils;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BaseFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public abstract class BaseFragment extends Fragment {

    @Nullable
    @BindView(R.id.progress)
    ProgressBar progressBar;

    OnFragmentInteractionListener mOnFragmentInteractionListener;
    OnListFragmentInteractionListener mOnListFragmentInteractionListener;

    private Unbinder unbinder;
    private boolean isLoading;

    public BaseFragment() {
        // Required empty public constructor
    }

    public boolean isLoading() {
        return isLoading;
    }

    protected void startProgress() {

        isLoading = true;
        UIUtils.setVisible(progressBar, true);
    }

    protected void endProgress() {

        isLoading = false;
        UIUtils.setVisible(progressBar, false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        Timber.tag(this.getClass().getSimpleName());
        Timber.d("onCreate");
    }

    @Override
    public void onAttach(Context context) {

        super.onAttach(context);

        if (context instanceof OnFragmentInteractionListener) {
            mOnFragmentInteractionListener = (OnFragmentInteractionListener) context;
        }

        if (context instanceof OnListFragmentInteractionListener) {
            mOnListFragmentInteractionListener = (OnListFragmentInteractionListener) context;
        }

        if (mOnFragmentInteractionListener == null && mOnListFragmentInteractionListener == null) {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener or mOnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {

        super.onDetach();
        mOnFragmentInteractionListener = null;
        mOnListFragmentInteractionListener = null;
    }

    @Override
    public void onDestroyView() {

        super.onDestroyView();
        butterknifeUnbind();
    }

    protected void butterknifeBind(Fragment fragment, View view) {
        unbinder = ButterKnife.bind(fragment, view);
    }

    protected void butterknifeUnbind() {
        if (unbinder != null)
            unbinder.unbind();
    }

    @Override
    public void onPause() {

        super.onPause();

        Timber.tag(this.getClass().getSimpleName());
        Timber.d("onPause");
    }

    @Override
    public void onResume() {

        super.onResume();

        Timber.tag(this.getClass().getSimpleName());
        Timber.d("onResume");
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {

        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {

        // TODO: Update argument type and name
        void onListFragmentInteraction(Object item);
    }
}
