package be.shouldit.showtime.ui.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;

import be.shouldit.showtime.R;
import be.shouldit.showtime.data.model.Presentation;
import be.shouldit.showtime.ui.FragmentsUtils;
import be.shouldit.showtime.ui.OnPresentationItemSelected;
import be.shouldit.showtime.ui.fragments.PresentationListFragment;
import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * Application Main Activity.
 */
public class ListPresentationActivity extends BaseActivity implements OnPresentationItemSelected
{
    private PresentationListFragment mFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(DRAWER_LAYOUT_ACTIVITY);
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        mFragment = PresentationListFragment.newInstance();
        FragmentManager fm = getSupportFragmentManager();
        FragmentsUtils.changeFragment(fm, R.id.fragment_container, mFragment, false);

        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayUseLogoEnabled(false);
        }

        Timber.d("Application ready");
    }

    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPresentationItemSelected(Presentation presentationItem) {

    }
}
