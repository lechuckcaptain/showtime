package be.shouldit.showtime.ui;

import be.shouldit.showtime.data.model.Presentation;

/**
 * Created by mpagliar on 12/08/2016.
 */
public interface PresentationUpdate
{
    Presentation get_selected();
    void onPresentationUpdated(Presentation presentation);
}
