package be.shouldit.showtime.ui;

import java.util.Date;
import java.util.Map;

import be.shouldit.showtime.data.model.Employee;

/**
 * Created by mpagliar on 12/08/2016.
 */
public interface OnEmployeeSelected
{
    void onEmployeeSelected(Map<Employee, Date> excludedMap);
}
