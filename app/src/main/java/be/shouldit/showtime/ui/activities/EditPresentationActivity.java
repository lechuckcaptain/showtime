package be.shouldit.showtime.ui.activities;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.github.paolorotolo.appintro.AppIntro;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import be.shouldit.showtime.App;
import be.shouldit.showtime.data.model.Employee;
import be.shouldit.showtime.data.model.Presentation;
import be.shouldit.showtime.ui.OnEmployeeSelected;
import be.shouldit.showtime.ui.PresentationUpdate;
import be.shouldit.showtime.ui.fragments.AbsentSelectionListFragment;
import be.shouldit.showtime.ui.fragments.BaseFragment;
import be.shouldit.showtime.ui.fragments.PresentationDetailsFragment;

/**
 * Application Main Activity.
 */
public class EditPresentationActivity extends AppIntro implements OnEmployeeSelected, PresentationUpdate, BaseFragment.OnFragmentInteractionListener
{
    public static final String SELECTED_PRESENTATION = "PRESENTATION";
    private Presentation _presentation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        showSkipButton(false);

        Bundle b = getIntent().getExtras();
        if(b != null && b.containsKey(SELECTED_PRESENTATION))
            _presentation = (Presentation) b.getSerializable(SELECTED_PRESENTATION);
        else
        {
            _presentation = new Presentation(new Date(),
                    App.getInstance().getEmployeeRepo().getAll(),
                    new HashMap<Employee,Date>());
        }

        addSlide(PresentationDetailsFragment.newInstance());
        addSlide(AbsentSelectionListFragment.newInstance());
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        // Do something when users tap on Skip button.
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);

        App.getInstance().getPresentationRepo().add(_presentation);
        finish();
    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
        super.onSlideChanged(oldFragment, newFragment);
        // Do something when the slide changes.
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public Presentation get_selected() {
        return _presentation;
    }

    @Override
    public void onPresentationUpdated(Presentation presentation) {

        _presentation = presentation;
    }

    @Override
    public void onEmployeeSelected(Map<Employee, Date> excludedMap) {

        _presentation.set_excluded(excludedMap);
    }
}
