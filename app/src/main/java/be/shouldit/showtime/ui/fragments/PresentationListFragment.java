package be.shouldit.showtime.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import be.shouldit.showtime.App;
import be.shouldit.showtime.R;
import be.shouldit.showtime.ui.OnPresentationItemSelected;
import be.shouldit.showtime.ui.activities.EditPresentationActivity;
import be.shouldit.showtime.ui.adapters.PresentationAdapter;
import be.shouldit.showtime.utils.UIUtils;
import butterknife.BindView;
import timber.log.Timber;

public class PresentationListFragment extends BaseFragment {

    @BindView(R.id.list)
    RecyclerView _recyclerView;

    @BindView(R.id.no_data_text)
    TextView _noDataText;

    @BindView(R.id.fragment_container)
    RelativeLayout _fragmentContainer;

    @BindView(R.id.add)
    FloatingActionButton _addFab;

    private LinearLayoutManager mLayoutManager;
    private PresentationAdapter mAdapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public PresentationListFragment() {

    }

    public static PresentationListFragment newInstance() {

        PresentationListFragment fragment = new PresentationListFragment();
        return fragment;
    }

    @Override
    protected void startProgress() {

        super.startProgress();

        UIUtils.setVisible(_recyclerView, false);
        UIUtils.setVisible(_noDataText, false);
    }

    @Override
    public void endProgress() {

        super.endProgress();

        boolean dataAvailable = mAdapter.getItemCount() > 0;

        UIUtils.setVisible(_recyclerView, dataAvailable);
        UIUtils.setVisible(_noDataText, !dataAvailable);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_presentation_list, container, false);
        butterknifeBind(this, view);

        setupUI();
        getActivity().invalidateOptionsMenu();

        return view;
    }

    private void setupUI() {

        final Context context = _recyclerView.getContext();
        mLayoutManager = new LinearLayoutManager(context);
        mAdapter = new PresentationAdapter((OnPresentationItemSelected) getActivity());
        _recyclerView.setAdapter(mAdapter);
        _recyclerView.setHasFixedSize(true);
        _recyclerView.setItemAnimator(new DefaultItemAnimator());
        _recyclerView.setLayoutManager(mLayoutManager);

        _addFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Timber.d("Add new presentation");

                Intent i = new Intent(context, EditPresentationActivity.class);
                startActivity(i);
            }
        });
    }

    @Override
    public void onResume() {

        super.onResume();
        startProgress();
        mAdapter.addAll(App.getInstance().getPresentationRepo().getAll());
        endProgress();
    }
}

