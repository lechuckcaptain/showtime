package be.shouldit.showtime.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import be.shouldit.showtime.R;
import be.shouldit.showtime.data.model.Presentation;
import be.shouldit.showtime.ui.OnPresentationItemSelected;
import be.shouldit.showtime.ui.viewholder.PresentationViewHolder;
import timber.log.Timber;

/**
 * Created by Marco on 11/06/16.
 */
public class PresentationAdapter extends RecyclerView.Adapter<PresentationViewHolder> {

    private final List<Presentation> presentationList;
    private final OnPresentationItemSelected onPresentationItemSelected;
    private final Context context;

    public PresentationAdapter(OnPresentationItemSelected onPresentationItemSelected) {

        this.context = (Context) onPresentationItemSelected;
        this.onPresentationItemSelected = onPresentationItemSelected;
        this.presentationList = new ArrayList<>();
    }

    public void addAll(List<Presentation> presentations) {

        reset();
        for (Presentation p : presentations) {
            if (!presentationList.contains(p)) {
                add(p);
            }
        }
    }

    public void add(Presentation presentation) {

        int position = getItemCount();

        if (!presentationList.contains(presentation)) {
            presentationList.add(presentation);
            notifyItemInserted(position);
            Timber.d("Size: %d items", getItemCount());
        }
    }

    @Override
    public PresentationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.fragment_presentation_list_item, parent, false);

        return new PresentationViewHolder(this, itemView, context);
    }

    @Override
    public void onBindViewHolder(PresentationViewHolder holder, int position) {

        PresentationViewHolder presentationViewHolder = (PresentationViewHolder) holder;
        Presentation presentation = presentationList.get(position);
        Timber.d("Setting Presentation viewholder: %s", presentation.toString());
        presentationViewHolder.setPresentation(presentation);
    }

    @Override
    public int getItemCount() {

        return presentationList.size();
    }

    public void remove(Presentation presentation) {

        int indexOfItem = presentationList.indexOf(presentation);

        if (indexOfItem != -1) {
            presentationList.remove(presentation);
            notifyItemRemoved(indexOfItem);
            Timber.d("PresentationAdatper size: %d items", getItemCount());
        }
    }

    public void reset() {
        presentationList.clear();
        notifyDataSetChanged();
    }
}
