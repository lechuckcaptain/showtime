package be.shouldit.showtime.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import be.shouldit.showtime.R;
import be.shouldit.showtime.data.model.Employee;
import be.shouldit.showtime.data.model.Presentation;
import be.shouldit.showtime.ui.OnEmployeeSelected;
import be.shouldit.showtime.ui.viewholder.EmployeeViewHolder;
import timber.log.Timber;

/**
 * Created by Marco on 11/06/16.
 */
public class EmployeeAdapter extends RecyclerView.Adapter<EmployeeViewHolder> {

    private final List<Employee> employeeList;
    private final Map<Employee,Date> excludedEmployees;
    private final OnEmployeeSelected onEmployeeSelected;
    private final Context context;

    public EmployeeAdapter(OnEmployeeSelected onPresentationItemSelected) {

        this.context = (Context) onPresentationItemSelected;
        this.onEmployeeSelected = onPresentationItemSelected;
        this.employeeList = new ArrayList<>();
        this.excludedEmployees = new HashMap<>();
    }

    public void addAll(List<Employee> employees) {

        for (Employee e : employees) {
            if (!employeeList.contains(e)) {
                add(e);
            }
        }
    }

    public void add(Employee employee) {

        int position = getItemCount();

        if (!employeeList.contains(employee)) {
            employeeList.add(employee);
            excludedEmployees.put(employee,null);
            notifyItemInserted(position);
            Timber.d("Size: %d items", getItemCount());
        }
    }

    public void setExcluded(Map<Employee, Date> excluded) {


    }

    @Override
    public EmployeeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.fragment_checkable_employee_list_item, parent, false);

        return new EmployeeViewHolder(this, itemView, context);
    }

    @Override
    public void onBindViewHolder(EmployeeViewHolder holder, int position) {

        holder.setEmployee(employeeList.get(position), isExcluded(position));
    }

    private boolean isExcluded(int position)
    {
        return isExcluded(employeeList.get(position));
    }

    private boolean isExcluded(Employee employee)
    {
        return excludedEmployees.get(employee) != null;
    }

    @Override
    public int getItemCount() {

        return employeeList.size();
    }

    public void remove(Presentation employee) {

        int indexOfItem = employeeList.indexOf(employee);

        if (indexOfItem != -1) {
            employeeList.remove(employee);
            notifyItemRemoved(indexOfItem);
            Timber.d("PresentationAdatper size: %d items", getItemCount());
        }
    }

    public void reset() {
        employeeList.clear();
        notifyDataSetChanged();
    }

    public void onEmployeeSelected(Employee employee) {

        Date value = excludedEmployees.get(employee);

        if (value != null) {
            excludedEmployees.put(employee, null);
        }
        else
        {
            excludedEmployees.put(employee, new Date());
        }

        notifyItemChanged(employeeList.indexOf(employee));
        onEmployeeSelected.onEmployeeSelected(excludedEmployees);
    }
}
