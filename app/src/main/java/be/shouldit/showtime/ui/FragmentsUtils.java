package be.shouldit.showtime.ui;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import be.shouldit.showtime.ui.activities.ListPresentationActivity;
import timber.log.Timber;

/**
 * Created by Marco on 22/06/13.
 */
public class FragmentsUtils {

    public static void goToMainActivity(Context context) {
        Intent mainIntent = new Intent(context, ListPresentationActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(mainIntent);
    }

    public static void changeFragment(FragmentManager fragmentManager, int frameId, Fragment fragment, boolean addToBackStack) {

        String newFragmentName = fragment.getClass().getName();
        String currentFragmentName = "";

        try {

            Fragment currentFragment = fragmentManager.findFragmentById(frameId);
            boolean fragmentPopped = fragmentManager.popBackStackImmediate(newFragmentName, 0);
            Timber.d("Popped fragment: '%b'", fragmentPopped);

            if (!fragmentPopped) {
                //Fragment not in back stack, create it.
                FragmentTransaction ft = fragmentManager.beginTransaction();

                String tag = fragment.getClass().getSimpleName();

//                TODO: Add animation to the transaction
//                ft.setCustomAnimations(enter, exit, pop_enter, pop_exit);

                if (currentFragment == null) {
                    Timber.d("Add fragment: '%s'", fragment);
                    ft.add(frameId, fragment, tag);
                } else {
                    Timber.d("Replace current with fragment: '%s'", fragment);
                    ft.replace(frameId, fragment, tag);

                    if (addToBackStack) {
                        Timber.d("Fragment added to back stack");
                        ft.addToBackStack(newFragmentName);
                    }
                }

                ft.commit();
            }

        } catch (IllegalStateException e) {
            Timber.e(e, "Unable to commit fragment, could be activity has been killed in background");
        }
    }
}
