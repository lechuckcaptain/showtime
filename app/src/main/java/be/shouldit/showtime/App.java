package be.shouldit.showtime;

import android.app.Application;

import be.shouldit.showtime.data.MockupEmployeeRepository;
import be.shouldit.showtime.data.IRepository;
import be.shouldit.showtime.data.MockPresentationRepository;
import be.shouldit.showtime.data.model.Employee;
import be.shouldit.showtime.data.model.Presentation;
import be.shouldit.showtime.utils.CrashlyticsTree;
import be.shouldit.showtime.utils.InitUtils;
import timber.log.Timber;

/**
 * Created by mpagliar on 26/10/2015.
 */
public class App extends Application {

    private static final Object lock = new Object();
    private static App instance;

    private boolean fabricSetupDone;

    public static App getInstance() {

        return instance;
    }

    public IRepository<Presentation> getPresentationRepo()
    {
        return pRepo;
    }

    public IRepository<Employee> getEmployeeRepo()
    {
        return eRepo;
    }

    private static IRepository<Presentation> pRepo;
    private static IRepository<Employee> eRepo;

    @Override
    public void onCreate() {

        super.onCreate();

        instance = this;

        Timber.plant(new CrashlyticsTree());

        fabricSetupDone = InitUtils.setupFabric(App.this);
        pRepo = new MockPresentationRepository();
        eRepo = new MockupEmployeeRepository();
    }
}
