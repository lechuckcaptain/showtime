package be.shouldit.showtime.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.IntDef;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import be.shouldit.showtime.R;
import timber.log.Timber;

public class UIUtils {
    public static void showError(Context ctx, int error) {
        try {
            showError(ctx, ctx.getString(error));
        } catch (Exception e) {
            Timber.e(e, "Exception on showError");
        }
    }

    public static void showError(Context ctx, String errorMessage) {
        try {
            showDialog(ctx, errorMessage, ctx.getString(R.string.attention));
        } catch (Exception e) {
            Timber.e(e, "Exception on showError");
        }
    }

    public static void showDialog(Context ctx, String message, String title) {
        try {
            AlertDialog.Builder builder =
                    new AlertDialog.Builder(ctx);

            builder.setTitle(title);
            builder.setMessage(message);
            builder.setPositiveButton("OK", null);
            builder.show();
        } catch (Exception e) {
            Timber.e(e, "Exception on showDialog");
        }
    }

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp      A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px      A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public static float convertPixelsToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return dp;
    }

    public static String CleanExclusion(String exclusion) {
        if (TextUtils.isEmpty(exclusion))
            return "";
        else {
            String[] splitted = exclusion.split(",");
            return TextUtils.join(", ", splitted);
        }
    }

    @Visibility
    public static int booleanToVisibility(boolean b) {
        if (b) {
            return View.VISIBLE;
        } else {
            return View.GONE;
        }
    }

    public static void setVisible(View v, boolean b) {
        if (v != null)
            v.setVisibility(booleanToVisibility(b));
    }

    public static int getTagsColor(Context ctx, int i) {
        int c;

        switch (i) {
            case 1:
                c = ctx.getResources().getColor(R.color.md_red_500);
                break;
            case 2:
                c = ctx.getResources().getColor(R.color.md_yellow_500);
                break;
            case 3:
                c = ctx.getResources().getColor(R.color.md_green_500);
                break;
            case 4:
                c = ctx.getResources().getColor(R.color.md_purple_500);
                break;
            case 5:
                c = ctx.getResources().getColor(R.color.md_blue_500);
                break;

            default:
                c = ctx.getResources().getColor(R.color.md_grey_500);
                break;
        }

        return c;
    }

    public static BitmapDrawable writeWarningOnDrawable(Context callerContext, int drawableId, String text) {
        return writeOnDrawable(callerContext, drawableId, text, Color.rgb(0xFF, 0xBB, 0x33));
    }

    public static BitmapDrawable writeErrorOnDrawable(Context callerContext, int drawableId, String text) {
        return writeOnDrawable(callerContext, drawableId, text, Color.rgb(0xFF, 0x44, 0x44));
    }

    public static BitmapDrawable writeErrorDisabledOnDrawable(Context callerContext, int drawableId, String text) {
        BitmapDrawable bd = writeOnDrawable(callerContext, drawableId, text, Color.RED);
        return bd;
    }

    public static BitmapDrawable writeOnDrawable(Context callerContext, int drawableId, String text, int color) {
        Bitmap bm = BitmapFactory.decodeResource(callerContext.getResources(), drawableId).copy(Bitmap.Config.ARGB_8888, true);

        Paint paint = new Paint();
        paint.setStyle(Style.FILL);
        paint.setColor(color);
        paint.setTypeface(Typeface.DEFAULT_BOLD);
        paint.setTextSize(20);

        /*
         *            W
         **************************
         *            *  	      *
         *            *  	      *
         *            *           *
         *            *           *
         *            *           *
         *            *           *
         **************************	H
         *            *  	      *
         *            *  	      *
         *            *     ##### *
         *            *     ##### *
         *            *     ##### *
         *            *    		  *
         **************************
         *
         */

        Canvas canvas = new Canvas(bm);

        int w = bm.getWidth();
        int h = bm.getHeight();

        int x0 = (int) (w * 0.65);
        int x1 = (int) (w * 0.99);
        int xr = (int) (w * 0.72);

        int y0 = (int) (h * 0.65);
        int y1 = (int) (h * 0.99);
        int yr = (int) (h * 0.94);

//		LogWrapper.d(TAG, String.format("W: %d; H: %d; ", w, h));
//		LogWrapper.d(TAG, String.format("x0: %d; x1: %d; xm: %d; y0: %d; y1: %d; ym: %d;", x0, x1, xr, y1, y0, yr));

        canvas.drawRect(new Rect(x0, y0, x1, y1), paint);
        paint.setColor(Color.WHITE);
        canvas.drawText(text, xr, yr, paint);

        BitmapDrawable bd = new BitmapDrawable(callerContext.getResources(), bm);
        return bd;
    }

    @IntDef({View.VISIBLE, View.INVISIBLE, View.GONE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Visibility {
    }
}
