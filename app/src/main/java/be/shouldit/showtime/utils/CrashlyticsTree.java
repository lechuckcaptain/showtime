package be.shouldit.showtime.utils;

import android.util.Log;

import com.crashlytics.android.Crashlytics;

import timber.log.Timber;

/**
 * Created by mpagliar on 26/10/2015.
 */
public final class CrashlyticsTree extends Timber.DebugTree {

    @Override
    protected void log(int priority, String tag, String message, Throwable t) {

        Crashlytics.log(priority, tag, t == null ? message : message + '\n' + Log.getStackTraceString(t));
    }
}
