package be.shouldit.showtime.utils;

import android.content.Context;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

/**
 * Created by mpagliar on 26/10/2015.
 */
public class InitUtils {

    public static boolean setupFabric(Context ctx) {

        Boolean setupDone;

        if (!Fabric.isInitialized()) {

            final Fabric fabric = new Fabric.Builder(ctx)
                    .kits(new Crashlytics())
                    .debuggable(true)
                    .build();
            Fabric.with(fabric);
        }

        setupDone = true;

        return setupDone;
    }
}
