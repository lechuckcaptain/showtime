package be.shouldit.showtime.data;

import java.util.List;

/**
 * Created by Marco on 12/02/2017.
 */

public interface IRepository<T>  {

    void add(T item);

    void remove(T item);

    void update(T item);

    List<T> getAll();

    T findById(int id);
}
