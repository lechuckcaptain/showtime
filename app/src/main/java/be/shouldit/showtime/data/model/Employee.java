package be.shouldit.showtime.data.model;

/**
 * Created by Marco on 11/02/2017.
 */

public class Employee extends PersistedItem {

    protected String _firstName;
    protected String _lastName;

    public Employee(String firstName, String lastName)
    {
        _firstName = firstName;
        _lastName = lastName;
    }

    public String get_firstName() {
        return _firstName;
    }

    public void set_firstName(String _firstName) {
        this._firstName = _firstName;
    }

    public String get_lastName() {
        return _lastName;
    }

    public void set_lastName(String _lastName) {
        this._lastName = _lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Employee)) return false;
        if (!super.equals(o)) return false;

        Employee employee = (Employee) o;

        if (get_firstName() != null ? !get_firstName().equals(employee.get_firstName()) : employee.get_firstName() != null)
            return false;
        return get_lastName() != null ? get_lastName().equals(employee.get_lastName()) : employee.get_lastName() == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (get_firstName() != null ? get_firstName().hashCode() : 0);
        result = 31 * result + (get_lastName() != null ? get_lastName().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return String.format("%s %s",get_firstName(),get_lastName());
    }
}
