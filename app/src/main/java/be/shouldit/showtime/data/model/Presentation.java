package be.shouldit.showtime.data.model;

import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by Marco on 11/02/2017.
 */

public class Presentation extends PersistedItem implements Comparable, Serializable {

    private List<Employee> _available;
    private Map<Employee,Date> _excluded;
    private Employee _presenter;
    private Date _date;

    public static Presentation NOT_FOUND;

    public Presentation(Date date, List<Employee> available, Map<Employee,Date> excluded)
    {
        _date = date;
        _available = available;

        if (excluded == null) {
            _excluded = new HashMap<>();
            for(Employee e : available) {
                excluded.put(e,null);
            }
        }
        else {
            _excluded = excluded;
        }

        UpdatePresenter();
    }

    private void UpdatePresenter() {
        Random r = new Random();
        while (true)
        {
            Employee e = _available.get(r.nextInt(_available.size()));
            if (_excluded.get(e) == null)
            {
                _presenter = e;
                break;
            }
        }
    }

    public Presentation(Date date, List<Employee> available)
    {
        this(date, available, null);
    }

    public List<Employee> get_available() {
        return _available;
    }

    public Map<Employee, Date> get_excluded() {
        return _excluded;
    }

    public void set_excluded(Map<Employee, Date> _excluded) {
        this._excluded = _excluded;
        UpdatePresenter();
    }

    public Date get_date() {
        return _date;
    }

    public void set_date(Date _date) {
        this._date = _date;
    }

    public Employee get_presenter() {
        return _presenter;
    }

    @Override
    public int compareTo(@NonNull Object o) {

        Presentation p = (Presentation) o;
        return this._date.compareTo(p._date);
    }
}
