package be.shouldit.showtime.data;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Random;

import be.shouldit.showtime.data.model.Employee;
import be.shouldit.showtime.data.model.Presentation;

/**
 * Created by Marco on 12/02/2017.
 */

public class MockPresentationRepository implements IRepository<Presentation> {

    private final List<Presentation> presentationList;
    private final IRepository personRepo;

    public MockPresentationRepository() {

        presentationList = new ArrayList<>();
        personRepo = new MockupEmployeeRepository();
        initMockData();
    }

    private Date getRandomDate() {
        long offset = Timestamp.valueOf("2015-01-01 00:00:00").getTime();
        long end = Timestamp.valueOf("2017-01-01 00:00:00").getTime();
        long diff = end - offset + 1;
        Date rand = new Date(offset + (long) (Math.random() * diff));
        return rand;
    }

    private void initMockData()
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        Random r = new Random();

        for(int i=0; i<5; i++)
        {
            List<Employee> employeeList = personRepo.getAll();

            calendar.set(Calendar.YEAR, 2000 + r.nextInt(18));
            calendar.set(Calendar.DAY_OF_YEAR, r.nextInt(365));
            calendar.set(Calendar.HOUR, r.nextInt(24));
            calendar.set(Calendar.MINUTE, r.nextInt(60));

            Map<Employee,Date> excludedMap = new HashMap<>();
            int excluded = r.nextInt(10);
            for(int e=0; e<excluded; e++){
                excludedMap.put(employeeList.remove(r.nextInt(employeeList.size())), getRandomDate());
            }

            presentationList.add(new Presentation(calendar.getTime(), employeeList, excludedMap));
        }

        UpdateSort();
    }

    private void UpdateSort() {

        Collections.sort(presentationList);
        Collections.reverse(presentationList);
    }

    @Override
    public void add(Presentation item) {

        presentationList.add(item);
        UpdateSort();
    }

    @Override
    public void remove(Presentation item) {

        presentationList.remove(item);
        UpdateSort();
    }

    @Override
    public void update(Presentation item) {

        int position = presentationList.indexOf(item);
        presentationList.remove(position);
        presentationList.add(position, item);
        UpdateSort();
    }

    @Override
    public List<Presentation> getAll() {

        return presentationList;
    }

    @Override
    public Presentation findById(int id) {

        for(Presentation p : presentationList){

            if (p.get_id() == id)
                return p;
        }

        throw new NoSuchElementException();
    }
}
