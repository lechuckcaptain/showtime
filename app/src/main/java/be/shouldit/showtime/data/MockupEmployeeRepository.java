package be.shouldit.showtime.data;

import java.util.ArrayList;
import java.util.List;

import be.shouldit.showtime.data.model.Employee;

/**
 * Created by Marco on 12/02/2017.
 */

public class MockupEmployeeRepository implements IRepository<Employee> {

    public MockupEmployeeRepository() {

    }

    @Override
    public void add(Employee item) {

    }

    @Override
    public void remove(Employee item) {

    }

    @Override
    public void update(Employee item) {

    }

    @Override
    public List<Employee> getAll() {

        List<Employee> pl = new ArrayList<>();

        for(int i=0; i<50; i++)
        {
            pl.add(new Employee("FirstName"+i,"LastName"+i));
        }

        return pl;
    }

    @Override
    public Employee findById(int id) {
        return null;
    }
}
