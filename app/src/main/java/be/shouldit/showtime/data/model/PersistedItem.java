package be.shouldit.showtime.data.model;

import java.io.Serializable;

/**
 * Created by Marco on 11/02/2017.
 */

public class PersistedItem implements Serializable {

    protected final int _id;

    private static int itemIdCounter = 0;

    public PersistedItem()
    {
        _id = itemIdCounter++;
    }

    public int get_id() {
        return _id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PersistedItem)) return false;

        PersistedItem that = (PersistedItem) o;

        return get_id() == that.get_id();
    }

    @Override
    public int hashCode() {
        return get_id();
    }
}
