package be.shouldit.showtime.ui.activities;

import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import be.shouldit.showtime.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class ListPresentationActivityTest {

    @Rule
    public ActivityTestRule<ListPresentationActivity> mActivityTestRule = new ActivityTestRule<>(ListPresentationActivity.class);

    @Test
    public void baseTest() throws IOException, InterruptedException {

        onView(withId(R.id.list)).perform(RecyclerViewActions.scrollToPosition(2));
    }
}
